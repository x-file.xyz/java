/*
 *  Copyright (c) 2017 Max Brito
 *  License: EUPL-1.2
 *  http://x-file.xyz
 */
package main;

import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Max Brito, Germany
 */
public class TestXfile {
    
    public TestXfile() {
    }

    @Test
    public void testCreating() throws Exception {
        final String text = "((someone)) prepared questions\n" +
            "Created a copy of ((our)) own ((OSS management))\n" +
            "Is there an approval ((process)) for ((Athene?)) > no\n" +
            "We found ((someone)) saying that a specific ((channel)) is approved\n";
        
        final String key = "mykey";
        
        XFile xFile = new XFile(text, key);
        Assert.assertTrue(xFile.isValid());
        System.out.println(xFile.getEncryptedText());
        
        // now do the decoding
        String encodedText = xFile.getEncryptedText();
        XFile xFile2 = XFile.decodeText(encodedText, key);
        Assert.assertTrue(xFile2 != null);
        Assert.assertTrue(xFile2.isValid());
        System.out.println("-------------------------");
        System.out.println(xFile2.getCleanText());
        Assert.assertTrue(xFile2.getCleanText().contains("channel"));
    }
}
