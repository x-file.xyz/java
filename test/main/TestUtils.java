/*
 *  Copyright (c) 2017 Max Brito
 *  License: EUPL-1.2
 *  http://x-file.xyz
 */
package main;

import java.util.ArrayList;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Max Brito, Germany
 */
public class TestUtils {
    
    public TestUtils() {
    }

   @Test
   public void testGetProtectedKeywords() throws Exception {
       String test1 = "((Tom)) was away from the office and saw ((Theresa))"
               + " walking down the park. He also saw his friend ((Mike)).";
        ArrayList<String> output = utils.getProtectedKeywords(test1);
        // expected number of found cases
        Assert.assertEquals(3, output.size());
        // get a specific case from the previous example
        Assert.assertEquals("((Mike))", output.get(2));
   }
}
