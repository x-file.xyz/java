/*
 *  Copyright (c) 2017 Max Brito
 *  License: EUPL-1.2
 *  http://x-file.xyz
 */
package main;

import java.util.ArrayList;
import org.junit.Assert;
import org.junit.Test;

/**
 *
 * @author Max Brito, Germany
 */
public class TestCyberSec {
    
    public TestCyberSec() {
    }

   @Test
   public void testEncrypt() throws Exception {
       String key = "1234567890";
       String text = "hello world";
       
        String output = cybersec.encrypt(text, key);
        Assert.assertEquals("EDtOZm6s+5o9Bn12FE25M6k==", output);
        // EDtOZm6s+5o9Bn12FE25M6k== // XXEncode
        // P5alyI4AH0LNzDERQEHYIw== // Base64
        
        // decypt the result
        String decrypted = cybersec.decrypt(output, key);
        Assert.assertEquals(text, decrypted);
        
        System.out.println(decrypted);
   }
   
    @Test
    public void testCreateXFile() throws Exception {
        final String text = "((someone)) prepared questions\n" +
            "Created a copy of ((our)) own ((OSS management))\n" +
            "Is there an approval ((process)) for ((Athene?)) > no\n" +
            "We found ((someone)) saying that a specific ((channel)) is approved\n";
        
        final String key = "mykey";
        
        String encodedText = cybersec.xFileEncode(text, key);
        Assert.assertTrue(encodedText.startsWith("███████"));
        System.out.println(encodedText);
        System.out.println("------------------------------------");
        String decodedText = XFile.decodeText(encodedText, key).getCleanText();
        System.out.println(decodedText);
        System.out.println("------------------------------------");
   }
    
   @Test
   public void testConvertCensorToPlainText() throws Exception {
       
        final String censoredPortion = "███████ prepared questions\n" +
            "Created a copy of ███ own ██████████████\n" +
            "Is there an approval ███████ for ███████ > no\n" +
            "We found ███████ saying that a specific ███████ is approved\n" +
            "\n" +
            "";
       
        ArrayList<String> items = new ArrayList();
        items.add("someone");
        items.add("our");
        items.add("OSS management");
        items.add("process");
        items.add("Athene?");
        items.add("someone");
        items.add("channel");
        
        String plainText = 
                cybersec.convertCensorToPlainText(censoredPortion, items);
        
        System.out.println(plainText);
   }
     
    
   
   
}
