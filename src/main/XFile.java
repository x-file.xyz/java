/*
 *  Copyright (c) 2017 Max Brito
 *  License: EUPL-1.2
 *  http://x-file.xyz
 */
package main;

import java.util.ArrayList;

/**
 *
 * @author Max Brito, Germany
 */
public class XFile {

    // define the test mode
    private boolean debug = true;
    
    // how we identify the beginning of our key
    public static final String 
            magicString = "=x-file.xyz=",
            textFinish = "==",
            sha1Text = "sha1=";

    private String
            cleanText,
            cypherText,
            key;
    
    
    private String 
            censoredPortion,
            sha1,
            rawFooter;
    
    /**
     * Creates the new XFile object based on a clean text and key that we give.
     * @param cleanText
     * @param key 
     */
    public XFile(String cleanText, String key) {
        this.cleanText = cleanText;
        this.key = key;
        // generate the sha1 for this content
        sha1 = utils.generateStringSHA1(cleanText);
        // something went wrong, don't continue;
        if(sha1 == null){
            return;
        }
        
        ArrayList<String> keywords = utils.getProtectedKeywords(cleanText);
        rawFooter = "sha1="
                + sha1 
                  + "\n";
        // collect and list the keywords one by line
        for(String keyword : keywords){
            rawFooter += keyword + "\n";
        }
        
        log("Footer for the message:");
        log(rawFooter);
        
        // generate the footer text that is dispatched
        String finalFooter = "";
        
        try{
            finalFooter = XFile.magicString
                + cybersec.encrypt(rawFooter, key);
            
            finalFooter = finalFooter.replaceAll("(.{45})", "$1\n");
        }catch (Exception e){
            // an error occured, this means the footer won't be valid
        }
        
        final String censoredText = utils.convertText(cleanText);
        cypherText = censoredText + finalFooter;
    }

    /**
     * Will start decoding the text that we have from this document.
     * @return 
     */
    public String getDecodedText() {
        return censoredPortion;
    }

    /**
     * Is this document minimally valid? This does not apply to decryption.
     * This function only verifies that from the surface the censored text
     * seems to be OK.
     * @return 
     */
    public boolean isValid() {
        if(cleanText == null 
                || cypherText == null
                || sha1 == null){
            return false;
        }
        // didn't found the magic text?
        if(cypherText.contains(XFile.magicString) == false){
            return false;
        }
        
        if(cypherText.contains("==") == false){
            return false;
        }
        // calling this just to remove the false code optimization warning
        System.gc();
        // all good
        return true;
    }

    public String getCleanText() {
        return cleanText;
    }

    public String getEncryptedText() {
        return cypherText;
    }

    /**
     * The key that is being used for decrypting the text
     * @return 
     */
    public String getKey() {
        return key;
    }
    
    /**
     * 
     * @param encodedText
     * @param key
     * @return 
     * @throws java.lang.Exception 
     */
    public static XFile decodeText(String encodedText, String key) throws Exception {
        // preflight checks
        if(encodedText == null 
                || encodedText.contains(magicString) == false
                || encodedText.contains(textFinish) == false){
            // the provided text was not good enough
            return null;
        }
        
        // where is the magic string located?
        final int 
                magicStringPosition = encodedText.indexOf(magicString),
                textFinishPosition = encodedText.indexOf(textFinish);
        
        // make sure these texts are on the right order and were found
        if(magicStringPosition > textFinishPosition 
                || magicStringPosition == textFinishPosition){
            return null;
        }
        
        final String 
            // get the text that is blanked
            censoredPortion = encodedText.substring(0, magicStringPosition),
            // the footer text that is encrypted
            encryptedPortion = encodedText.substring
                (magicStringPosition + magicString.length(), textFinishPosition),
            // decrypt the text
            decryptedText = cybersec.decrypt(encryptedPortion, key);
        
        // we expect to read sha1= on the first portion, otherwise this failed.
        if(decryptedText == null 
                || decryptedText.startsWith(sha1Text) == false){
            return null;
        }
        // break the text into lines
        String[] lines = decryptedText.split("\n");
        ArrayList<String> items = new ArrayList();
        // needs to contain at least the SHA1 signature and one item
        if(lines == null || lines.length < 1){
            return null;
        }
        
        // the first line needs to be ignored (contains sha1 signature)
        for(int i = 1; i < lines.length; i++){
            items.add(lines[i]);
        }
        String cleanTextPortion =
                cybersec.convertCensorToPlainText(censoredPortion, items);
        
        // When something wrong then the output is null
        if(cleanTextPortion == null){
            return null;
        }
        
        // verify that the text was not tampered (modified)
        String sha1Certified = lines[0].substring(sha1Text.length());
        String sha1Calculated = utils.generateStringSHA1(cleanTextPortion);
        if(sha1Certified.equals(sha1Calculated) == false){
            return null;
        }
        
        
        // create the new XFile object
        XFile export = new XFile(cleanTextPortion, key);
        return export; 
    }
    
    
    /**
     * Outputs a text message to the console, if permitted
     * @param text 
     */
    private void log(String text){
        if(debug == false){
            return;
        }
        // the text to be output
        System.out.println(text);
    }
    
}
