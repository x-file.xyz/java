/*
 *  Copyright (c) 2017 Max Brito
 *  License: EUPL-1.2
 *  http://x-file.xyz
 */
package main;

import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Arrays;
import javax.crypto.Cipher;
import javax.crypto.spec.SecretKeySpec;

/**
 *
 * @author Max Brito, Germany
 * note: the encrypt and decrypt methods were copied from StackOverflow
 */
public class cybersec {
    
    private static final String ALGORITHM = "AES";
    
    /**
     * Encrypts the given plain text
     *
     * @param text
     * @param key
     * @return 
     * @throws java.lang.Exception
     */
    public static String encrypt(String text, String key) throws Exception{
        String validKey = makeKey128bits(key);
        byte[] byteKey = validKey.getBytes(StandardCharsets.UTF_8);
        byte[] byteText = text.getBytes(StandardCharsets.UTF_8);
        
        SecretKeySpec secretKey = new SecretKeySpec(byteKey, ALGORITHM);
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.ENCRYPT_MODE, secretKey);
        
        byte[] encrypted = cipher.doFinal(byteText);
        
        String encodedPortion = Base64.encodeXX(encrypted) + "==";
        //encodedPortion = encodedPortion.replaceAll("(.{45})", "$1\n");
        
        return encodedPortion;
    }
    
    /**
     * Decrypts the given byte array
     *
     * @param encodedMessage
     * @param key
     * @return 
     * @throws java.lang.Exception
     */
    public static String decrypt(String encodedMessage, String key) throws Exception{
        String validKey = makeKey128bits(key);
        byte[] byteKey = validKey.getBytes(StandardCharsets.UTF_8);
        byte[] cipherText = Base64.decodeXX(encodedMessage);
        SecretKeySpec secretKey = new SecretKeySpec(byteKey, ALGORITHM);
        Cipher cipher = Cipher.getInstance(ALGORITHM);
        cipher.init(Cipher.DECRYPT_MODE, secretKey);

        String result = new String(cipher.doFinal(cipherText), "UTF-8");
        return result;
    }
    
    /**
     * Returns a key that is sized on 128 bits as required for AES.
     * The main purpose is surface encryption, not a deep encryption.
     * @param key
     * @return 
     */
    public static String makeKey128bits(String key){
        final int size = 16; // 16 x8 = 128
        // if the key is too big, return the key truncated
        if(key.length()>  size){
            return key.substring(0, size);
        }
        char[] chars = new char[size - key.length()];
        Arrays.fill(chars, '#');
        return key + new String(chars);
    }
    
    /**
     * Looks for the portions of text covered in ((example)) to generate the
     * encrypted output.
     * @param text
     * @param key
     * @return 
     * @throws java.lang.Exception 
     */
    public static String xFileEncode(String text, String key) throws Exception{
        ArrayList<String> keywords = utils.getProtectedKeywords(text);
        String footer = "" //+ "\n" 
                + "sha1=123213213213213123" 
                + "\n";
        // collect and list the keywords one by line
        for(String keyword : keywords){
            footer += keyword + "\n";
        }
        // generate the footer text that is dispatched
        String finalFooter = 
                "\n"+ XFile.magicString
                + encrypt(footer, key);
        
        String censoredText = utils.convertText(text);
        return censoredText + finalFooter;
    } 
 
    /**
     * Given a censored text and an array of items that fill up that text, we
     * provided a clean text version of what is being stored.
     * @param censoredText
     * @param items
     * @return 
     */
    public static String convertCensorToPlainText(String censoredText, 
            ArrayList<String> items){
        String text = censoredText;
        int counter = 0;
        
        while(text.contains("█")){
            int pos = text.indexOf("█");
            String item = "((" +items.get(counter) + "))";
            text = 
                    text.substring(0, pos) // beginning of text
                    +  item // the text to replace
                    + text.substring(pos + items.get(counter).length()); // rest of text
            counter++; // increase the counter
        }
        
        return text;
    }
    
    
}
