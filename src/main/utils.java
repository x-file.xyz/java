/*
 *  Copyright (c) 2017 Max Brito
 *  License: EUPL-1.2
 *  http://x-file.xyz
 */
package main;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Max Brito, Germany
 */
class utils {

    /**
     * Convert the text into super duper secrete
     * @param text
     * @return 
     */
    static String convertText(String text) {
        String data = text;
        Matcher m = Pattern.compile("\\(\\((.*?)\\)\\)").matcher(text);
        while(m.find()) {
            String snippet = "((" + m.group(1) + "))";
            char[] chars = new char[m.group(1).length()];
            Arrays.fill(chars, '█');
            String censor = new String(chars);
            data = data.replace(snippet, censor);
        }
        return data;
    }

    /**
     * Looks at a given text and returns an array with the words that are
     * involved in double parenthesis. The order of these keywords on the array
     * follows the order when there were found on normal left to right, up to
     * down writing pattern.
     * @param text
     * @return 
     */
    public static ArrayList<String> getProtectedKeywords(String text) {
        ArrayList<String> data = new ArrayList();
        Matcher m = Pattern.compile("\\(\\((.*?)\\)\\)").matcher(text);
        while(m.find()) {
            String snippet = m.group(1);
            data.add(snippet);
        }
        return data;
    }
    
        /**
     * Generates an hash from a given string
     * @param content
     * @return 
     * @throws java.security.NoSuchAlgorithmException 
    */
    public static String generateStringSHA1(final String content){
        
        try {
            // Create the checksum digest
            MessageDigest md = MessageDigest.getInstance("SHA-1");
            md.update(content.getBytes());
            // get the converted bytes
            final byte byteData[] = md.digest();
            
            //convert the byte to hex format
            @SuppressWarnings("StringBufferMayBeStringBuilder")
            final StringBuffer hexString = new StringBuffer();
            for (int i=0;i<byteData.length;i++) {
                String hex=Integer.toHexString(0xff & byteData[i]);
                if(hex.length()==1) hexString.append('0');
                hexString.append(hex);
            }
            return hexString.toString();
        } catch (NoSuchAlgorithmException ex) {
        }
        return null;
    }
    
}
